acme>=2.1.0
ConfigArgParse>=0.9.3
configobj>=5.0.6
cryptography>=2.5.0
distro>=1.0.1
josepy>=1.13.0
parsedatetime>=2.4
pyrfc3339
pytz>=2019.3
setuptools>=41.6.0

[:sys_platform == "win32"]
pywin32>=300

[all]
azure-devops
ipdb
poetry>=1.2.0
poetry-plugin-export>=1.1.0
twine
Sphinx>=1.2
sphinx_rtd_theme
coverage
mypy
pip
pytest
pytest-cov
pytest-xdist
setuptools
tox
types-pyOpenSSL
types-pyRFC3339
types-pytz
types-requests
types-setuptools
types-six
typing-extensions
wheel

[all:python_full_version >= "3.7.2"]
pylint

[dev]
azure-devops
ipdb
poetry>=1.2.0
poetry-plugin-export>=1.1.0
twine

[docs]
Sphinx>=1.2
sphinx_rtd_theme

[test]
coverage
mypy
pip
pytest
pytest-cov
pytest-xdist
setuptools
tox
types-pyOpenSSL
types-pyRFC3339
types-pytz
types-requests
types-setuptools
types-six
typing-extensions
wheel

[test:python_full_version >= "3.7.2"]
pylint
